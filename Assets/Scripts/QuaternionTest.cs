﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomMath
{
    public struct QuaternionTest : IEquatable<QuaternionTest>
    {
        #region Variables

        public float x;
        public float y;
        public float z;
        public float w;

        public QuaternionTest normalized => Normalize(this);
        public Vec3 eulerAngles
        {
            get => ToEulerRad(this) * Mathf.Rad2Deg;
            set => this = Euler(value);
        }

        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;
                    case 1:
                        return y;
                    case 2:
                        return z;
                    case 3:
                        return w;
                    default:
                        throw new IndexOutOfRangeException("Invalid Quaternion index!");
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        break;
                    case 1:
                        y = value;
                        break;
                    case 2:
                        z = value;
                        break;
                    case 3:
                        w = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException("Invalid Quaternion index!");
                }
            }
        }

        #endregion

        #region Constants

        public const float kEpsilon = 1e-05f;

        #endregion

        #region Constructors

        public QuaternionTest(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public QuaternionTest(QuaternionTest q)
        {
            x = q.x;
            y = q.y;
            z = q.z;
            w = q.w;
        }

        #endregion

        #region Default Values

        public static QuaternionTest Identity { get; } = new QuaternionTest(0f, 0f, 0f, 1f);

        #endregion

        #region Operators

        public static QuaternionTest operator *(QuaternionTest lhs, QuaternionTest rhs)
        {
            return new QuaternionTest(
                lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y,
                lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z,
                lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x,
                lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z);
        }

        public static Vec3 operator *(QuaternionTest rotation, Vec3 point)
        {
            float x = rotation.x * 2f;
            float y = rotation.y * 2f;
            float z = rotation.z * 2f;
            float xx = rotation.x * x;
            float yy = rotation.y * y;
            float zz = rotation.z * z;
            float xy = rotation.x * y;
            float xz = rotation.x * z;
            float yz = rotation.y * z;
            float wx = rotation.w * x;
            float wy = rotation.w * y;
            float wz = rotation.w * z;

            Vec3 res;
            res.x = (1f - (yy + zz)) * point.x + (xy - wz) * point.y + (xz + wy) * point.z;
            res.y = (xy + wz) * point.x + (1f - (xx + zz)) * point.y + (yz - wx) * point.z;
            res.z = (xz - wy) * point.x + (yz + wx) * point.y + (1f - (xx + yy)) * point.z;
            return res;
        }

        public static bool operator ==(QuaternionTest lhs, QuaternionTest rhs)
        {
            return IsEqualUsingDot(Dot(lhs, rhs));
        }

        public static bool operator !=(QuaternionTest lhs, QuaternionTest rhs)
        {
            return !(lhs == rhs);
        }

        public static implicit operator QuaternionTest(Quaternion q) => new QuaternionTest(q.x, q.y, q.z, q.w);

        public static implicit operator Quaternion(QuaternionTest q) => new Quaternion(q.x, q.y, q.z, q.w);

        #endregion

        #region Functions

        public void Set(float newX, float newY, float newZ, float newW)
        {
            x = newX;
            y = newY;
            z = newZ;
            w = newW;
        }

        public void Set(Quaternion q)
        {
            x = q.x;
            y = q.y;
            z = q.z;
            w = q.w;
        }

        private static bool IsEqualUsingDot(float dot)
        {
            return dot > 1.0f - kEpsilon;
        }

        public static float Dot(QuaternionTest a, QuaternionTest b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        public static QuaternionTest Inverse(QuaternionTest rotation)
        {
            return new QuaternionTest(-rotation.x, -rotation.y, -rotation.z, rotation.w);
        }

        public static float Angle(QuaternionTest a, QuaternionTest b)
        {
            float dot = Dot(a, b);
            return IsEqualUsingDot(dot) ? 0.0f : Mathf.Acos(Mathf.Min(Mathf.Abs(dot), 1.0F)) * 2.0F * Mathf.Rad2Deg;
        }

        public static QuaternionTest AngleAxis(float angle, Vec3 axis)
        {
            if (axis.sqrMagnitude == 0.0f)
                return Identity;

            QuaternionTest result = Identity;
            float radians = angle * Mathf.Deg2Rad;
            radians *= 0.5f;
            axis.Normalize();
            axis = axis * Mathf.Sin(radians);
            result.x = axis.x;
            result.y = axis.y;
            result.z = axis.z;
            result.w = Mathf.Cos(radians);

            return Normalize(result);
        }

        public void ToAngleAxis(out float angle, out Vec3 axis)
        {
            angle = 0.0f;
            axis = Vec3.Zero;

            float radians = Mathf.Acos(w); 
            
            axis.x = x;
            axis.y = y;
            axis.z = z;
            axis = axis / Mathf.Sin(radians);
            axis.Normalize();

            radians /= 0.5f;
            angle = radians * Mathf.Rad2Deg;
        }

        public static QuaternionTest Lerp(QuaternionTest a, QuaternionTest b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            return LerpUnclamped(a, b, t);
        }

        public static QuaternionTest LerpUnclamped(QuaternionTest a, QuaternionTest b, float t)
        {
            QuaternionTest q = Identity;
            if (Dot(a, b) < 0)
            {
                q.x = a.x + t * (-b.x - a.x);
                q.y = a.y + t * (-b.y - a.y);
                q.z = a.z + t * (-b.z - a.z);
                q.w = a.w + t * (-b.w - b.w);
            }
            else
            {
                q.x = a.x + t * (b.x - a.x);
                q.y = a.y + t * (b.y - a.y);
                q.z = a.z + t * (b.z - a.z);
                q.w = a.w + t * (b.w - b.w);
            }
            return q.normalized;
        }

        public static QuaternionTest Slerp(QuaternionTest a, QuaternionTest b, float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            return SlerpUnclamped(a, b, t);
        }

        public static QuaternionTest SlerpUnclamped(QuaternionTest a, QuaternionTest b, float t)
        {
            float cosAngle = Dot(a, b);
            if (cosAngle < 0)
            {
                cosAngle = -cosAngle;
                b = new QuaternionTest(-b.x, -b.y, -b.z, -b.w);
            }
            float t1, t2;
            if (cosAngle < 0.95f)
            {
                float angle = Mathf.Acos(cosAngle);
                float sinAgle = Mathf.Sin(angle);
                float invSinAngle = 1 / sinAgle;
                t1 = Mathf.Sin((1 - t) * angle) * invSinAngle;
                t2 = Mathf.Sin(t * angle) * invSinAngle;
                return new QuaternionTest(a.x * t1 + b.x * t2, a.y * t1 + b.y * t2, a.z * t1 + a.z * t2, a.w * t1 + b.w * t2);
            }
            else
            {
                return Lerp(a, b, t);
            }
        }

        public static QuaternionTest FromToRotation(Vec3 fromDirection, Vec3 toDirection)
        {
            return RotateTowards(LookRotation(fromDirection), LookRotation(toDirection), float.MaxValue);
        }
        public void SetFromToRotation(Vec3 fromDirection, Vec3 toDirection)
        {
            this = FromToRotation(fromDirection, toDirection);
        }

        public static QuaternionTest RotateTowards(QuaternionTest from, QuaternionTest to, float maxDegreesDelta)
        {
            float t = Mathf.Min(1f, maxDegreesDelta / Angle(from, to));
            return SlerpUnclamped(from, to, t);
        }

        public void SetLookRotation(Vec3 view)
        {
            Vec3 up = Vec3.Up;
            SetLookRotation(view, up);
        }

        public void SetLookRotation(Vec3 view, Vec3 up)
        {
            this = LookRotation(view, up);
        }

        public static QuaternionTest LookRotation(Vec3 forward)
        {
            Vec3 up = Vec3.Up;
            return LookRotation(forward, up);
        }

        public static QuaternionTest LookRotation(Vec3 forward, Vec3 upwards)
        {
            forward = Vec3.Normalize(forward);
            Vec3 right = Vec3.Normalize(Vec3.Cross(upwards, forward));
            upwards = Vec3.Cross(forward, right);
            float m00 = right.x;
            float m01 = right.y;
            float m02 = right.z;
            float m10 = upwards.x;
            float m11 = upwards.y;
            float m12 = upwards.z;
            float m20 = forward.x;
            float m21 = forward.y;
            float m22 = forward.z;

            float num8 = (m00 + m11) + m22;
            QuaternionTest quaternion = new QuaternionTest();
            if (num8 > 0f)
            {
                float num = Mathf.Sqrt(num8 + 1f);
                quaternion.w = num * 0.5f;
                num = 0.5f / num;
                quaternion.x = (m12 - m21) * num;
                quaternion.y = (m20 - m02) * num;
                quaternion.z = (m01 - m10) * num;
                return quaternion;
            }
            if ((m00 >= m11) && (m00 >= m22))
            {
                float num7 = Mathf.Sqrt(((1f + m00) - m11) - m22);
                float num4 = 0.5f / num7;
                quaternion.x = 0.5f * num7;
                quaternion.y = (m01 + m10) * num4;
                quaternion.z = (m02 + m20) * num4;
                quaternion.w = (m12 - m21) * num4;
                return quaternion;
            }
            if (m11 > m22)
            {
                float num6 = Mathf.Sqrt(((1f + m11) - m00) - m22);
                float num3 = 0.5f / num6;
                quaternion.x = (m10 + m01) * num3;
                quaternion.y = 0.5f * num6;
                quaternion.z = (m21 + m12) * num3;
                quaternion.w = (m20 - m02) * num3;
                return quaternion;
            }
            float num5 = Mathf.Sqrt(((1f + m22) - m00) - m11);
            float num2 = 0.5f / num5;
            quaternion.x = (m20 + m02) * num2;
            quaternion.y = (m21 + m12) * num2;
            quaternion.z = 0.5f * num5;
            quaternion.w = (m01 - m10) * num2;
            return quaternion;
        }

        public static QuaternionTest Euler(float x, float y, float z)
        {
            QuaternionTest qx = Identity;
            QuaternionTest qy = Identity;
            QuaternionTest qz = Identity;

            float sinAngle = 0.0f;
            float cosAngle = 0.0f;

            sinAngle = Mathf.Sin(Mathf.Deg2Rad * y * 0.5f);
            cosAngle = Mathf.Cos(Mathf.Deg2Rad * y * 0.5f);
            qy.Set(0, sinAngle, 0, cosAngle);

            sinAngle = Mathf.Sin(Mathf.Deg2Rad * x * 0.5f);
            cosAngle = Mathf.Cos(Mathf.Deg2Rad * x * 0.5f);
            qx.Set(sinAngle, 0, 0, cosAngle);

            sinAngle = Mathf.Sin(Mathf.Deg2Rad * z * 0.5f);
            cosAngle = Mathf.Cos(Mathf.Deg2Rad * z * 0.5f);
            qz.Set(0, 0, sinAngle, cosAngle);

            return qy * qx * qz;
        }

        public static QuaternionTest Euler(Vec3 euler)
        {
            return Euler(euler.x, euler.y, euler.z);
        }

        public static Vec3 ToEulerRad(QuaternionTest rotation)
        {
            float sqw = rotation.w * rotation.w;
            float sqx = rotation.x * rotation.x;
            float sqy = rotation.y * rotation.y;
            float sqz = rotation.z * rotation.z;
            float unit = sqx + sqy + sqz + sqw;
            float test = rotation.x * rotation.w - rotation.y * rotation.z;
            Vec3 v;

            if (test > 0.4995f * unit)
            {
                v.y = 2f * Mathf.Atan2(rotation.y, rotation.x);
                v.x = Mathf.PI / 2;
                v.z = 0;
                return NormalizeAngles(v * Mathf.Rad2Deg);
            }
            if (test < -0.4995f * unit)
            {
                v.y = -2f * Mathf.Atan2(rotation.y, rotation.x);
                v.x = -Mathf.PI / 2;
                v.z = 0;
                return NormalizeAngles(v * Mathf.Rad2Deg);
            }
            QuaternionTest q = new QuaternionTest(rotation.w, rotation.z, rotation.x, rotation.y);
            v.y = Mathf.Atan2(2f * q.x * q.w + 2f * q.y * q.z, 1 - 2f * (q.z * q.z + q.w * q.w));
            v.x = Mathf.Asin(2f * (q.x * q.z - q.w * q.y));
            v.z = Mathf.Atan2(2f * q.x * q.y + 2f * q.z * q.w, 1 - 2f * (q.y * q.y + q.z * q.z));
            return NormalizeAngles(v * Mathf.Rad2Deg);
        }

        private static Vec3 NormalizeAngles(Vec3 angles)
        {
            angles.x = NormalizeAngle(angles.x);
            angles.y = NormalizeAngle(angles.y);
            angles.z = NormalizeAngle(angles.z);
            return angles;
        }
        private static float NormalizeAngle(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;
            return angle;
        }

        public static QuaternionTest Normalize(QuaternionTest q)
        {
            float mag = Mathf.Sqrt(Dot(q, q));

            if (mag < kEpsilon)
                return Identity;

            return new QuaternionTest(q.x / mag, q.y / mag, q.z / mag, q.w / mag);
        }

        public void Normalize()
        {
            this = Normalize(this);
        }

        #endregion

        #region Internals

        public bool Equals(QuaternionTest other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return x.Equals(other.x) && y.Equals(other.y) && z.Equals(other.z) && w.Equals(other.w);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((QuaternionTest)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = x.GetHashCode();
                hashCode = (hashCode * 397) ^ y.GetHashCode();
                hashCode = (hashCode * 397) ^ z.GetHashCode();
                hashCode = (hashCode * 397) ^ w.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"({x:F1}, {y:F1}, {z:F1}, {w:F1})";
        }

        public string ToString(string format)
        {
            return string.Format("({0}, {1}, {2}, {3})", x.ToString(format), y.ToString(format), z.ToString(format), w.ToString(format));
        }
        #endregion
    }
}