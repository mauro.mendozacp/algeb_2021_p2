﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

public class Test : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float speed = 1f;

    void Start()
    {
        
    }

    void Update()
    {
        float step = speed * Time.deltaTime;

        float kEpsilon = 1e-05f;
        float dot = QuaternionTest.Dot(transform.rotation, target.rotation);
        if (dot > 1.0f - kEpsilon)
        {
            print(dot);
        }
    }
}
