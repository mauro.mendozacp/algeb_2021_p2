﻿using System;
using System.Collections;
using System.Collections.Generic;
using CustomMath;
using EjerciciosAlgebra;
using MathDebbuger;
using UnityEngine;

public class EjerciciosTest : MonoBehaviour
{
    public Ejercicio ejercicio;
    public float angle;

    public enum Ejercicio
    {
        Uno,
        Dos,
        Tres
    }

    void Start()
    {
        Vector3Debugger.AddVector(new Vec3(10f, 0.0f, 0.0f), Color.red, "Ejer1");

        List<Vec3> pos1List = new List<Vec3>();
        pos1List.Add(new Vec3(10f, 0.0f, 0.0f));
        pos1List.Add(new Vec3(10f, 10f, 0.0f));
        pos1List.Add(new Vec3(20f, 10f, 0.0f));
        Vector3Debugger.AddVectorsSecuence(pos1List, false, Color.blue, "Ejer2");

        List<Vec3> pos2List = new List<Vec3>();
        pos2List.Add(new Vec3(10f, 0.0f, 0.0f));
        pos2List.Add(new Vec3(10f, 10f, 0.0f));
        pos2List.Add(new Vec3(20f, 10f, 0.0f));
        pos2List.Add(new Vec3(20f, 20f, 0.0f));
        Vector3Debugger.AddVectorsSecuence(pos2List, false, Color.yellow, "Ejer3");
    }
    
    void FixedUpdate()
    {
        TurnOffAll();
        switch (ejercicio)
        {
            case Ejercicio.Uno:
                TurnOn("Ejer1");
                Vec3 newPos1 = new Vec3();
                newPos1 = QuaternionTest.Euler(new Vec3(0.0f, angle, 0.0f)) * Vector3Debugger.GetVectorsPositions("Ejer1")[1];
                Vector3Debugger.UpdatePosition("Ejer1", newPos1);

                break;
            case Ejercicio.Dos:
                TurnOn("Ejer2");
                List<Vec3> newPos2List = new List<Vec3>();
                for (int i = 0; i < Vector3Debugger.GetVectorsPositions("Ejer2").Count; ++i)
                    newPos2List.Add(QuaternionTest.Euler(new Vec3(0.0f, angle, 0.0f)) * Vector3Debugger.GetVectorsPositions("Ejer2")[i]);
                Vector3Debugger.UpdatePositionsSecuence("Ejer2", newPos2List);

                break;
            case Ejercicio.Tres:
                TurnOn("Ejer3");
                List<Vec3> newPos3List = new List<Vec3>();
                newPos3List.Add(Vector3Debugger.GetVectorsPositions("Ejer3")[0]);
                newPos3List.Add(QuaternionTest.Euler(new Vec3(angle, angle, 0.0f)) * Vector3Debugger.GetVectorsPositions("Ejer3")[1]);
                newPos3List.Add(Vector3Debugger.GetVectorsPositions("Ejer3")[2]);
                newPos3List.Add(QuaternionTest.Euler(new Vec3(-angle, -angle, 0.0f)) * Vector3Debugger.GetVectorsPositions("Ejer3")[3]);
                newPos3List.Add(Vector3Debugger.GetVectorsPositions("Ejer3")[4]);
                Vector3Debugger.UpdatePositionsSecuence("Ejer3", newPos3List);

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void TurnOffAll()
    {
        TurnOff("Ejer1");
        TurnOff("Ejer2");
        TurnOff("Ejer3");
    }

    private void TurnOff(string id)
    {
        Vector3Debugger.TurnOffVector(id);
        Vector3Debugger.DisableEditorView(id);
    }

    private void TurnOn(string id)
    {
        Vector3Debugger.TurnOnVector(id);
        Vector3Debugger.EnableEditorView(id);
    }
}
